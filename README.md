#这是一个spring retry相关的测试工程
- [ ] test

- [ ] add new
- [ ] testnew
https://my.oschina.net/wangjunBlog/blog/1889015

[aws cli sqs](https://docs.aws.amazon.com/cli/latest/reference/sqs/send-message.html)


# aws cli sqs send multiple messages  
## 命令 
aws sqs send-message-batch --queue-url https://sqs.ap-northeast-1.amazonaws.com/923430801426/testqueue --entries file://send-message-batch.json

## send-message-batch.json文件的内容 
``` 
[
     {
       "Id": "test01",
           "MessageBody": "test01",
           "MessageAttributes": {}
     },
     {
       "Id": "test02",
           "MessageBody": "test02",
           "MessageAttributes": {}
     }
]
```
## excel文件的公式  
```
="{"&CHAR(10)&"       |Id|: |"&A1&"|,"&CHAR(10)&"           |MessageBody|: |"&B1&"|,"&CHAR(10)&"           |MessageAttributes|: {}"&CHAR(10)&"     },"
```

### 生成的内容拷贝到sakura里面，类似这样
- 替换双引号  
- 替换|为双引号
- 去掉最后面的逗号
- 用[]把所有内容括起来
```
"{
       |Id|: |a|,
           |MessageBody|: |b|,
           |MessageAttributes|: {}
     },"
"{
       |Id|: |a|,
           |MessageBody|: |b|,
           |MessageAttributes|: {}
     },"
```


https://blog.csdn.net/kaka20099527/article/details/53508403?depth_1-utm_source=distribute.pc_relevant.none-task&utm_source=distribute.pc_relevant.none-task


# powershell 批量生成文件内容
- powershell中字符串转义使用`(反引号，unix或者 linux中执行命令的)
```
New-Item -path "c:\\temp" -Name "test2.csv"
for ($i=1;$i -le 10000;$i++){
     Write-Output "`"json_key`":`"json_value_${i}`"" >> "c:\\temp\test2.csv" 
}
```