package com.example.demo.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RetryServiceTest  {

    @Autowired
    private RetryService retryService;

    @Test
    public void retry() {
        int count = retryService.retry(-1);
        System.out.println("库存为 ：" + count);
    }
}